---
layout: handbook-page-toc
title: "Account Team"
description: "The account team works together to drive value, success, and growth for a customer"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

- - -

The account team works together to drive value, success, and growth for a customer.

## Enterprise

An Enterprise account team is comprised of the following people:

- [Strategic Account Leader (SAL)](/job-families/sales/strategic-account-leader/)
- [Solutions Architect (SA)](/job-families/sales/solutions-architect/)
- [Technical Account Manager (TAM)](/job-families/sales/technical-account-manager/) for [qualifying accounts](/handbook/customer-success/tam/services/#tam-alignment)

This is commonly referred to as the "SAL-SA-TAM" team (sometimes also written as "SALSATAM").

## Commercial/Mid-Market

A Mid-Market account team is comprised of the following people:

- [Account Executive (AE)](/job-families/sales/account-executive/)
- [Technical Account Manager (TAM)](/job-families/sales/technical-account-manager/) for [qualifying accounts](/handbook/customer-success/tam/services/#tam-alignment)

In Mid-Market, Solutions Architects are pooled so they are not aligned with specific account teams.

## Account Team Meeting

The account team should meet regularly to review their accounts. Some common topics to discuss during an account team meeting include:

- The status of accounts in general, particularly those with a [health score other than Green](/handbook/customer-success/tam/health-score-triage/#health-assessment-guidelines)
- Open opportunities for growth, upgrade, and/or renewal of existing customers
  - Coordinated customer growth/expansion strategies
- Open opportunities for new business
- The status of onboarding for recently added customers

These meetings should be scheduled on a recurring basis, for the team to review their accounts and make sure they are aligned on their efforts. A weekly cadence is recommended, but if there is a small number of accounts or the accounts are low-touch, a biweekly cadence may be more appropriate.

TAMs should leverage their Gainsight TAM Portfolio dashboard to open discussion and dialogue. 

Account teams may choose to include the [Sales Development Representative (SDR)](/job-families/marketing/sales-development-representative/) in their account meetings. The SDR primarily focuses on generating new business so they are not necessarily involved in existing customer accounts, but it may be beneficial to keep everyone informed across the entire account development lifecycle.

To view more information about Account Team Meetings, please refer to the [SALSATAM Page](/handbook/customer-success/account-team/SALSATAM/) 
## AE/SA-TAM Account Handoff and Overlap

When an account moves from pre-sales to post-sales, it is [handed off from the Account Executive/Solutions Architect to the Technical Account Manager](/handbook/customer-success/pre-sales-post-sales-transition).
