---
layout: job_family_page
title: "Vice President, Internal Audit"
---

## Vice President, Internal Audit
The Vice President, Internal Audit reports directly to the chairman of the audit committee with a dotted line day to day control and administrative reporting relationship with the Chief Finance Officer. The Vice President, Internal Audit will be responsible for overall preparation and execution of a risk-based audit plan to assess, report on, and recommend improvements to the company’s key operational and finance activities and testing of internal controls. Additionally, the position is responsible for the Enterprise Risk Management (ERM) control environment and the initial identifying and assisting in documenting existing internal finance and disclosure controls, implementing and documenting new internal controls, and establishing an internal monitoring function to audit the company’s compliance with such internal controls.  Once documented, ownership for and changing internal control procedures will reside in the owner of the control.
The ongoing testing and control review, including the ERM environment will be controlled by the Internal audit group.   The position will have a key role in assessing the company’s compliance with the requirements of the Sarbanes-Oxley Act of 2002, along with the PAO organization. The position will be further called on to identify and implement finance department process improvements. Once documented, ownership for and changing internal control procedures will reside in the owner of the control.

### Job Grade
The Vice President, Internal Audit is a [grade #12](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Responsibilities
* Overall responsibility for the Yearly Internal Audit Plan approved by the Audit Committee and quarterly updates and reporting package to the Audit Committee.
* Initial documentation of GitLab SOX controls, processes, and recommends additional controls where control deficiencies are identified.
* Is the subject matter expert on controls with GitLab business partners, audit committee, and C-Suite executives.
* Build and manage a high functioning, distributed team of direct reports for operational and financial risk audits.
* Manages an intern program to bring on new personnel to train and to fit into the accounting/finance functions.
* Works with the Legal and Technical Accounting teams to identify related party companies from inquiries from the Board and C-Suite team.
* Identify, understand, and document processes and procedures surrounding the ERM and internal control areas. Continually monitor and update the assessment of the control environment, keeping abreast of significant control issues, trends and developments.
* Develop test plans and coordinate the performance of management testing of internal controls required by Sarbanes-Oxley.
* Identify and assess the implementation of new controls as needed.
* Responsible for conducting investigations and/or review of areas as directed by the Audit Committee and/or WhistleBlower events or Fraud identification.
* Prepare and update a comprehensive risk-based audit plan in coordination with the Audit Committee for evaluating and testing the effectiveness of controls in place to manage significant risk exposures, ensure the integrity and reliability of information and financial reporting, safeguard company assets, and comply with laws and regulations.
* Identify and design anti-fraud criteria and controls.
* Establish procedures and plan for conducting internal control audits for financial risks or operational efficiency.
* Report findings to senior management and the company’s audit committee.
* Understand the requirements of the Sarbanes-Oxley Act of 2002 (and any related SEC pronouncements) and assist in maintaining processes and functions to help ensure compliance with such requirements, working with the PAO organization.
* Coordinate activities with external auditors to support their audit and review procedures.
* Participate in disclosure committee meetings.
* Review finance department business processes and suggest ways to improve such processes.
Other duties, as directed by the Audit Committee and/or the Chief Financial Officer:
* Identify and Review Financial Risks within the Company.
* Develop Audit plans that will be approved by the Audit Committee. 
* Handle other audits or reviews as directed by the Audit Committee or Chief Financial Officer.

## Requirements
* Ability to use GitLab
* Previous management experience; ability to contribute to the career development of staff and a culture of teamwork and adherence to the Culture within GitLab.
* Comprehensive knowledge of auditing practices, procedures, and principles, sufficient to interpret and analyze complex concepts and apply them in innovative ways. Skills and knowledge should include an extensive understanding of financial, operational, market and credit risk. Should have expert knowledge of generally accepted auditing standards in the US.
* Capability and desire to evaluate the effectiveness of management in their stewardship of GitLab’s resources and their compliance with established corporate policy and procedures, including corporate governance, code of conduct standards, and business ethics and conduct policy.
* Creative approaches and solutions necessary to solve complex problems.
* Strong written and verbal communication skills with experience interacting with and presenting to senior management-level personnel.
* The candidate must have excellent interpersonal skills and will serve as a member of the senior management team.
* He/she should be driven to deliver quality results on time, with a high degree of integrity, in a highly ethical and professional manner.
* The candidate should be self-reliant and have strong initiative as well as possess solid business judgment.
* He/she must be resourceful and strategic and possess excellent analytical abilities.
* Able to utilize Best Practices on recommendations and audit findings.
* Have a mix of public and software industry experience. 
* CPA and previous experience as an executive.

### Performance Indicators
* Development of comprehensive audit plans
* ERM experience
* Completing tasks and audits timely and efficiently
* Utilizing Best Practices related to audit findings and recommendations.
* Approval ratings based on surveys above 80% in the first year, moving to 90% in subsequent years}

### Career Ladder
The next step in the Vice President Internal Audit job family is to move to a higher level role which is not yet defined at GitLab.

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.

* Next, candidates will be invited to schedule a 45 minute interview with our PAO
* Next, candidates will be invited to schedule a 45 minute interview with our CFO.
* Next, candidates will be invited to schedule a 45 minute interview with our CLO.
* Next, candidates will be invited to interview with our Audit Partner.
* Finally, candidates will be invited to interview with the Chairman of the Audit Committee.
Successful candidates will subsequently be made an offer via phone and email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).

## Director, Internal Audit
The Director, Internal Audit reports directly to the chairman of the audit committee with a dotted line day to day control and administrative reporting relationship with the principal accounting officer. The Director, Internal Audit will be responsible for preparing and implementing a risk-based audit plan to assess, report on, and recommend improvements to the company’s key operational and finance activities and internal controls. Additionally, the position is responsible for identifying and assisting in documenting existing internal finance and disclosure controls, implementing and documenting new internal controls, and establishing an internal monitoring function to audit the company’s compliance with such internal controls. The position will have a key role in assessing the company’s compliance with the requirements of the Sarbanes-Oxley Act of 2002. The position will be further called on to identify and implement finance department process improvements. Once documented, ownership for and changing internal control procedures will reside in the owner of the control.
The Director, Internal Audit reports to the Vice President, Internal Audit.

### Job Grade
The Director, Internal Audi} is a [grade #10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
* Drives the reporting package to the Audit Committee.
* Documents GitLab SOX controls, processes, and recommends additional controls where there are control deficiencies.
* Is the subject matter expert on controls with GitLab business partners, audit committee, and C-Suite executives.
* Build and manage a highly functioning, distributed team of direct reports.
* Manages an intern program to bring on new personnel to train and to fit into the accounting/finance functions.
* Works with the Legal and Technical Accounting teams to identify related party companies from inquiries from the Board and C-Suite team.
* Identify, understand, and document processes and procedures surrounding internal controls. Continually monitor and update the assessment of the control environment, keeping abreast of significant control issues, trends and developments.
* Develop test plans and coordinate the performance of management testing of internal controls required by Sarbanes-Oxley.
* Identify and assess the implementation of new controls as necessary.
* Prepare and update a comprehensive risk-based audit plan for evaluating the effectiveness of controls in place to manage significant risk exposures, ensure the integrity and reliability of information and financial reporting, safeguard company assets, and comply with laws and regulations.
* Identify and design anti-fraud criteria and controls.
* Monitor and audit the company’s compliance with established internal controls.
* Establish procedures and plan for conducting internal control audits.
* Report findings to senior management and the company’s audit committee.
* Understand the requirements of the Sarbanes-Oxley Act of 2002 (and any related SEC pronouncements) and assist in maintaining processes and functions to help ensure compliance with such requirements.
* Coordinate activities with external auditors to support their audit and review procedures.
* Participate in disclosure committee meetings.
* Review finance department business processes and suggest ways to improve such processes.
Other duties, as directed by the Audit Committee and/or the Principal Accounting Officer:
* Identify and Review Financial Risks within the Company.
* Develop Audit plans that will be approved by the Audit Committee to review 1-3 Financial Risks audits and report findings first to the area related to these Financial Risks and report final reports to the Audit Committee.
* Handle other audits or reviews as directed by the Audit Committee, CFO or PAO.

### Requirements
* Ability to use GitLab
* Previous management experience; ability to contribute to the career development of staff and a culture of teamwork.
* Comprehensive knowledge of auditing practices, procedures, and principles, sufficient to interpret and analyze complex concepts and apply them in innovative ways. Skills and knowledge should include an extensive understanding of financial, operational, market and credit risk. Should have expert knowledge of generally accepted auditing standards in the US.
* Capability and desire to evaluate the effectiveness of management in their stewardship of GitLab’s resources and their compliance with established corporate policy and procedures, including corporate governance, code of conduct standards, and business ethics and conduct policy.
*Creative approaches and solutions necessary to solve complex problems.
*Strong written and verbal communication skills with experience interacting with and presenting to senior management-level personnel.
* The candidate must have excellent interpersonal skills and will serve as a member of the senior management team.
* The candidate should be an energetic, entrepreneurial self-starter capable of self-direction.
* He/she should be driven to deliver quality results on time, with a high degree of integrity, in a highly ethical and professional manner.
* The candidate should be self-reliant and have strong initiative as well as possess solid business judgment.
* He/she must be resourceful and strategic and possess excellent analytical abilities.
* Able to utilize Best Practices on recommendations and audit findings.
* A minimum of ten years of experience in a public accounting firm and or software industry with a heavy emphasis on financial and accounting applications and financial and operational controls.
* Experience in an internal audit function preferred.
* Experience in the software industry highly preferred.
* Bachelor’s degree in accounting or related finance field. Chartered accountant (CA) or certified public accountant (CPA) desirable.
* Certified internal auditor (CIA) or certified information systems auditor (CISA) preferred.

### Performance Indicators
* [Percentage of Desktop procedures documented](https://about.gitlab.com/handbook/internal-audit/#internal-audit-performance-measures)
* [Percentage of controls tested](https://about.gitlab.com/handbook/internal-audit/#internal-audit-performance-measures)
* [Percentage of recommendations implemented](https://about.gitlab.com/handbook/internal-audit/#internal-audit-performance-measures) 
* [Percentage of audits completed](https://about.gitlab.com/handbook/internal-audit/#internal-audit-performance-measures)
* [New Hire Location Factor < 0.69](https://about.gitlab.com/handbook/business-technology/metrics/#new-hire-location-factor--069)
* Completing tasks and audits timely and efficiently
* Utilizing Best Practices related to audit findings and recommendations.
* Approval ratings based on surveys above 80% in the first year, moving to 90% in subsequent years.

### Career Ladder
The next step in the Internal Audit job family is to move to the [PAO](https://about.gitlab.com/job-families/finance/pao-jf/) job family.

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 45 minute interview with our Controller.
* Candidates will then be invited to schedule a 45 minute interview with our CFO.
* Finally, candidates will interview with the Chairman of the Audit Committee.
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
