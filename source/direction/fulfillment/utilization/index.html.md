---
layout: markdown_page
title: "Group Direction - Utilization"
description: This is the direction page for the Utilization group which is part of the Fulfillment stage. Learn more here!
canonical_path: "/direction/fulfillment/utilization/"
---

- TOC
{:toc}

Last reviewed: 2021-08-18

### Introduction and how you can help
Thanks for visiting the direction page for the Utilization group within the Fulfillment Section in GitLab. This page is maintained by [Amanda Rueda](https://gitlab.com/amandarueda), the Product Manager for the Utilization Group. This direction page is a work in progress and sharing your feedback directly on issues and epics within GitLab.com is the best way to contribute.


### North Star
The Utilization group within the Fulfillment Section has a focus on helping customers to manage their budgets. Specifically, we aim to provide customers with insight and tools to manage usage of seats and consumables, like CI minutes and Storage. 

### Who do we serve?
Although some features may cater specifically to large enterprises, the majority of our work benefits customers of all sizes and sectors. The typical [user persona](/handbook/marketing/strategic-marketing/roles-personas/#user-personas) interacting with our features is [Sidney (Systems Administrator)](/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator). 


#### Engineering

The Fulfillment sub-department handbook page has a [Utilization section](/handbook/engineering/development/fulfillment/#utilization) and our engineering team members directory can be found [here](/handbook/engineering/development/fulfillment/be-utilization/#team-members).

#### Challenges to address

Today, customers struggle to effectively manage costs, especially when it comes to user overages due to a lack of:

1. Visibility
1. Tools 
1. System automation

We're leaning heavily into solving for Visibility issues now, and look forward to expanding into tooling and system automations for the remainder of the fiscal year.

#### Roadmap

We utilize these themes to focus our short-term roadmap and ensure we're picking work which solves a core customer problem. 

Our current roadmap is focused on:

| Theme | Feature | Segment | Product | 
|---|---|---|---|
|Visibility | [Billable members](https://gitlab.com/groups/gitlab-org/-/epics/4547) | All | SaaS | 
|Visibility | [Billable users](https://gitlab.com/groups/gitlab-org/-/epics/5505) | All | Self-managed  | 
|Visibility | [Seats Usage Vision](https://gitlab.com/groups/gitlab-org/-/epics/5872) | All | SaaS & Self-Managed |
|Visibility | [Improved visibility into storage consumption](https://gitlab.com/groups/gitlab-org/-/epics/5490) | All | SaaS & Self-Managed |
|Tools|[Give admins control over accidental user overages via a "User Cap" setting](https://gitlab.com/groups/gitlab-org/-/epics/4315)|All|SaaS & Self-Managed |
|System automation | [Automatically deactivate dormant users](https://gitlab.com/groups/gitlab-org/-/epics/5519) | Enterprise | Self-managed  |




